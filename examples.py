from pyxlread.cls import ExcelData
from pyxlread.xlread import get_info_from_excel
import timeit




## EXAMPLES ---------------

# Timing ----
code = """
from pyxlread.cls import ExcelData
from pyxlread.xlread import get_info_from_excel
info = get_info_from_excel(file = "C:/Users/fches/Documents/trial_python_extraction_01.xlsx", header_row=(0,0))
"""

elapsed_time = timeit.timeit(stmt=code, number=100)/100*1000 # time in ms
print(elapsed_time)

# Run example ----
#info = get_info_from_excel(file = "C:/Users/fches/Documents/trial_python_extraction_01.xlsx", header_row=(0,0))
#print(info)
#print(info.get_infos(sheet="order"))
#print(info.get_infos(sheet="details"))
#print(info.get_data(sheet="details"))
#print(info.get_data(sheet="details", dataframe=True))
#info.common_fields()
#info.sheets_with_common_header()
#info.get_data_per_column(sheet_name="details", column_name="sample_id")
#info.merge_data()

info2 = get_info_from_excel(file = "C:/Users/fches/Documents/trial_python_extraction_02.xlsx", header_row=(0,0,0))
#print(info2)
#print(info2.get_infos(sheet="order"))
#print(info2.get_infos(sheet="details"))
#print(info2.get_data(sheet="details"))
#print(info2.get_data(sheet="details", dataframe=True))
#info2.common_fields()
info2.common_fields(sheets=("details", "more_details"))
info2.sheets_with_common_header()
#info2.get_data_per_column(sheet_name="details", column_name="sample_id")
info2.merge_data(sheets=("details", "more_details"))
#info2.merge_data()