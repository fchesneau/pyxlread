import os
from xlrd import open_workbook
from pandas import DataFrame, merge, read_excel
import collections

# Class will only get data as output in the get_info_from_excel() function.
# slots: ws_names
# col_names: dictionary with sheet names as key
# data: dictionary with sheet names as key

class ExcelData:

    def __init__(self, filepath, sh_names, col_names, data = None, info = None): # add number of sheets, nrow and ncol per sheet, min cols per sheet. All in 1 dictionary?
        self.filepath = filepath
        self.filename = os.path.basename(filepath)
        self.fileinfo = os.stat(filepath)
        self.sheet_names = sh_names # tuple of sheet names
        self.column_names = col_names # dictionary with sheet name as key and tuple of column names as values
        self.data = data # dictionary with sheet name as kex and list of tuples as values
        self.info = info # dictionary of dictionries containing infos about each worksheet

    def __repr__(self):
        return "Object id: %s\nObject class: ExcelData\n\
                Object content:\n\
                - filepath: %s \n\
                - sheet_names:%s \n\
                - column names:%s\n" % (id(self), self.filepath, self.sheet_names, self.column_names)

    ## Checks ----

   ## Class Methods ----
    def __find_common_header(self, sheets=None): #TODO(): Change implementation to a dictionary: {"sheet name": ([other sheets], [common columns])
        dict_col = self.column_names
        ws_names = list(dict_col.keys())

        all_col_names = []
        if sheets is not None:
            for sh in sheets:
                all_col_names.append(dict_col[sh])
        else:
            all_col_names = list(dict_col.values())

        flat_list = [item for sublist in all_col_names for item in sublist]
        duplicates = [item for item, count in collections.Counter(flat_list).items() if count > 1]
        return duplicates

    def __find_header_in_worksheet(self, headers):
        # Method to find worksheets having common
        output = []
        dict_cols = self.column_names
        for key in dict_cols:
            print(dict_cols[key])
            print(tuple(headers) in dict_cols[key])
            #TODO(): If statement with tuple of values does not work.
            if set(headers).issubset(dict_cols[key]):
                output.append(key)
        output = list(set(output)) # temporary fix. If sheet names given, list of sheets contains duplicates
        return output

    ## Accessors ----

    def get_filename(self):
        return self.filename

    def get_filepath(self):
        return self.filepath

    def get_sheetnames(self):
        return self.sheet_names

    def get_colnames(self):
        return self.column_names

    def get_infos(self, sheet=None):
        if sheet is None:
            return self.info
        else:
            return self.info[sheet]
    
    def get_data(self, sheet=None, columns=None, dataframe=False):
        data = self.data[sheet]
        col_names = self.column_names[sheet]

        if dataframe:
            df = DataFrame(data=data)
            df.columns = col_names
            return df
        else:
            return (col_names, data)
    
    def get_data_per_column(self, sheet_name, column_name, dataframe=False):

        if type(sheet_name) is tuple and len(sheet_name) > 1:
            print("Only single values are accepted for the sheet_name argument.\nUsing the first item...")
            sheet_name = sheet_name[0]
        
        if type(column_name) is tuple and len(column_name) > 1:
            print("Only single values are accepted for the column_name argument.\nUsing the first item...")
            column_name = column_name[0]

        if sheet_name not in self.sheet_names:
            print("Wrong sheet name given.\nExiting now...")

        if column_name not in self.column_names[sheet_name]:
            print("Wrong column name given.\nExiting now...")
        
        sheet = tuple(self.column_names[sheet_name])
        idx = sheet.index(column_name)

        return [item[idx] for item in self.data[sheet_name]]

    def merge_data(self, sheets=None):
        com_fields = tuple(self.common_fields(sheets=sheets))
        com_sheets = self.sheets_with_common_header()
        
        print(com_fields)
        print(com_sheets)

        data = []
        for sh in com_sheets:
            data.append(self.get_data(sheet=sh, dataframe=True))

        output = None
        for idx in range(len(data)):
            if idx == 0:
                output = data[0]
            else:
                output = merge(data[idx], output, on=com_fields)
        
        return output


    ## Other methods ----
    def common_fields(self, sheets=None):
        return self.__find_common_header(sheets=sheets)
    
    def sheets_with_common_header(self, sheets=None):
        hdrs = self.__find_common_header(sheets=sheets)
        return self.__find_header_in_worksheet(headers=hdrs)
