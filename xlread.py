import os
from xlrd import open_workbook
from pandas import DataFrame, merge, read_excel
from pyxlread.cls import ExcelData

def get_info_from_excel(file=None, sheets = ("order", "details"), header_row = 0): # header_row: if one value use same value for all sheets.

    # Load workbook
    wb = open_workbook(filename = file)
    ws_names = wb.sheet_names()
    
    # Perform checks
    if all(isinstance(elt, str) for elt in sheets) == False:
        print("Please provide a string or tuple of strings for the sheet name(s).\n" + "Exiting now...\n")
        exit()
        
    if sheets != None and all(elt in ws_names for elt in sheets) == False:
        print("One or more wrong names provided in the 'sheets' argument.\n" + "Exiting now...\n")
        exit()

    # Get sheets properties and data
    columns = {}
    data = {}
    info = {"n_worksheets": wb.nsheets, "worksheet_names": ws_names}
    ii = 0
    for sh in ws_names:
        wsh = wb.sheet_by_name(sh)
        nrow, ncol = wsh.nrows, wsh.ncols
        # get position of headers for each sheet.
        if type(header_row) is not int:
            if len(header_row) > 1:
                hrow = header_row[ii]
                ii += 1
            else:
                hrow = header_row[0]
        else:
            hrow = header_row

        if nrow < hrow:
            print("The header row is out of range of the number of rows found.\n\
                    Ensure that the header row is withing the range of the data.\n\
                    Exiting now...")
            exit()

        columns[sh] = tuple(wsh.row_values(rowx = hrow, start_colx = 0, end_colx = ncol))
        data[sh] = [tuple(wsh.row_values(rowx = row, start_colx=0, end_colx=ncol)) for row in range((hrow+1),nrow)]
        info[sh] = {"sheet_name": sh, "nrow": nrow, "ncol": ncol, "header_position": hrow}
        
        

    # Create ExcelData object and return output
    output = ExcelData(filepath=file, sh_names=ws_names, col_names=columns, data=data, info=info)
    
    return(output)

def get_info_from_excel_pandas(filename, format = "tuple"):
    data = read_excel(filename)
    shape = data.shape

    # Return the proper output type.
    if format == "dataframe":
        return data
    elif format == "tuple":
        tp = [tuple(row) for row in data.values]
        tp.append(tuple(data.columns))
        return tp
    else:
        print("Format not recognised.\nRecognised formats are 'tuple' and 'dataframe'.\nExiting now...")
        exit()
