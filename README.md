# python_excel_read

A few functions and classes to deal with excel files.
The idea is to return an object containing the structure and the data of an excel workbook.
Uses the xlrd library under the hood (https://pypi.org/project/xlrd/).